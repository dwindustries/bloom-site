export default function fadeOut(el){
    // Pure JS Fade out
    el.style.opacity = 1;
        (function fade(){
        if((el.style.opacity -= .1) < 0){
            el.style.display="none"; 
        } else {
            requestAnimationFrame(fade);
        }
    })();
}