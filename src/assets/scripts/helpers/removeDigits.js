// Simple function to trim digits from a number
export default function removeDigits(x, n){
    return (x-(x%Math.pow(10, n)))/Math.pow(10, n) 
}