import '../sections/product';

import $ from 'jquery';
import sections from '@shopify/theme-sections';
import toggle from '../helpers/toggle';
import floatingCTA from '../components/floating-cta';

$(document).ready(() => {
  sections.load('product');

  // Product dropdown toggle
  toggle('.js-info-trigger', '.js-info-content');  

  // Floating cart toggle
  var floatingEl = document.querySelector('.js-floating-cta');
  if(floatingEl) { floatingCTA(); }
});