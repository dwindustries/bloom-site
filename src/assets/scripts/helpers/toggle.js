import $ from 'jquery';

export default function toggle(trigger, content) {
  if (trigger !== '.js-nav-trigger') {
    $(content).hide();
  } else if (window.innerWidth < 746) {
    $(content).hide();
  }

  $(trigger).each(function(){
    $(this).click((event) => {
      event.preventDefault();

      var $toggleContent = $(this).parent().find(content);
  
      if ($toggleContent.hasClass('active')) {
        // Close this element
        $(this).removeClass('active');
        $toggleContent.removeClass('active').slideUp();
      } else {
        // Close all elements
        var $old = $(trigger).parent().find(content);
        $(trigger).removeClass('active');
        $old.removeClass('active').slideUp();

        // Open new active el
        $(this).addClass('active');
        $toggleContent.addClass('active').slideDown();
      }
    });
  });

}
