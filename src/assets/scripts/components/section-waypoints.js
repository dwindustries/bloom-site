import './noframework.waypoints.js';

export default function sectionWaypoints() {
	const sections = document.querySelectorAll('.js-section-waypoint');

	for(let i = 0; i < sections.length; i++) {
		sections[i].id = 'js-section-waypoint'+i;
		applyAnims('js-section-waypoint'+i);
	}            

   function applyAnims(id) {
	   const container = document.getElementById(id);
	   let animateEls;	
	   animateEls = container.querySelectorAll('.js-waypoint-animate');

		for(let j=0; j < animateEls.length; j++){
			animateEls[j].style.opacity = 0;
		}

	   new Waypoint({
		   element: container,
		   handler: function(direction) {

			   for(let k=0; k < animateEls.length; k++){
				   let animationType = animateEls[k].dataset.animation;
				   animateEls[k].classList.add('animated');
				   animateEls[k].classList.add(animationType);
			   }
		   },
		   offset: '40%',
		   continuous: false,
	   })

	   	// Reveal section borders
		const borderTrigger = document.querySelector('#js-section-waypoint0');
		const borderContainer = document.querySelector('.site-wrapper');
		new Waypoint({
		element: borderTrigger,
		handler: function(direction) {
				if (direction == 'down') {
					borderContainer.classList.add('reveal');
				}
				else {
					borderContainer.classList.remove('reveal');
				}
			},
			offset: '50%',
		})
	}

}

