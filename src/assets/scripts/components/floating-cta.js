import './noframework.waypoints.js';

export default function floatingCTA() {
    var container = document.querySelector('#js-floating-cta-trigger');
    var el = document.querySelector('.js-floating-cta');
    var form = document.querySelector('.single-product__selection');
    var formContent = document.querySelector('.floating-cta__content');
    var formOrigin = document.querySelector('.single-product__content');

    new Waypoint({
        element: container,
        handler: function(direction) {     
            formContent.appendChild(form);
            if (direction == 'down') {
                formContent.appendChild(form);
                el.classList.add('reveal');
            }
            else {
                el.classList.remove('reveal');
                formOrigin.appendChild(form);
            }
        },
        offset: '0%',
    });
}