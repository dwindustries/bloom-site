import 'lazysizes/plugins/object-fit/ls.object-fit';
import 'lazysizes/plugins/parent-fit/ls.parent-fit';
import 'lazysizes/plugins/rias/ls.rias';
import 'lazysizes/plugins/bgset/ls.bgset';
import 'lazysizes';
import 'lazysizes/plugins/respimg/ls.respimg';

import '../../styles/theme.scss';
import '../../styles/theme.scss.liquid';

import $ from 'jquery';
// import {pageLinkFocus} from '@shopify/theme-a11y';
import {cookiesEnabled} from '@shopify/theme-cart';
import {wrapTable, wrapIframe} from '@shopify/theme-rte';
import toggle from '../helpers/toggle';
import ajaxifyCart from '../components/ajaxify-cart';
import carousel from '../components/carousel';
import sectionWaypoints from '../components/section-waypoints';
import announcement from '../components/announcement';

window.slate = window.slate || {};
window.theme = window.theme || {};

$(document).ready(() => {
  // Common a11y fixes
  // if (window.location.hash !== '#') {
  //   pageLinkFocus($(window.location.hash));
  // }

  // $('.in-page-link').on('click', (evt) => {
  //   pageLinkFocus($(evt.currentTarget.hash));
  // });

  // Target tables to make them scrollable
  const tableSelectors = '.rte table';

  wrapTable({
    $tables: $(tableSelectors),
    tableWrapperClass: 'rte__table-wrapper',
  });

  // Target iframes to make them responsive
  const iframeSelectors =
	'.rte iframe[src*="youtube.com/embed"],' +
	'.rte iframe[src*="player.vimeo"]';

  wrapIframe({
	$iframes: $(iframeSelectors),
	iframeWrapperClass: 'rte__video-wrapper',
  });

  // Apply a specific class to the html element for browser support of cookies.
  if (cookiesEnabled()) {
    document.documentElement.className = document.documentElement.className.replace(
      'supports-no-cookies',
      'supports-cookies',
    );
  }

  // Nav dropdown toggle
  toggle('.js-nav-trigger', '.js-nav-body');

  // swiper carousel
  carousel();

  // Section flipper
  const sectionSwapEl = document.querySelector('.js-section-waypoint');
  if(sectionSwapEl) { sectionWaypoints(); }

  // Link Block
  function linkBlock(){
    $('.js-clickable').css('cursor','pointer');
    $(".js-clickable").click(function() {
        window.location = $(this).find("a").attr("href"); 
        return false;
    });
  }
  if($('.js-clickable').length){ linkBlock(); }

  // FAQ dropdown toggle
  if($('.js-faq-trigger').length){
    toggle('.js-faq-trigger', '.js-faq-content');  
  }

  // Fade Out the overlay
  var $screen = $('.js-site-overlay');

  if($('.js-site-overlay').length){
    (function(){
        setTimeout(function(){
            $screen.addClass('hide');
        }, 1000);

        setTimeout(function(){
            $screen.fadeOut('slow');
        }, 2000, function(){
            $screen.remove();
        });

    })(); 
  
    // Announcement func
    if($('.js-announcement').length){
      announcement();
    }
  }

  ajaxifyCart();

  $('.js-mc-input').focusin(function(){
    $('.js-mc-label').addClass('label-shift');
  });  

});