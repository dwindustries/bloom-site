export default function carousel() {

  const carousel = new Swiper('.js-carousel', {
	slidesPerView: 3,
	spaceBetween: 30,
	breakpoints: {
		600: {
			slidesPerView: 1,
			spaceBetween: 20,
		},
		1100: {
			slidesPerView: 2,
			spaceBetween: 15,
		}
	},
	navigation: {
		nextEl: '.quotes-nav--next',
		prevEl: '.quotes-nav--prev',
	},
	});

}
