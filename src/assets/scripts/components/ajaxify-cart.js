  /**
  * Module to ajaxify all add to cart forms on the page.
  *
  * Copyright (c) 2015 Caroline Schnapp (11heavens.com)
  * Dual licensed under the MIT and GPL licenses:
  * http://www.opensource.org/licenses/mit-license.php
  * http://www.gnu.org/licenses/gpl.html
  *
  */

import $ from 'jquery';
import jQuery from 'jquery';
import { count } from 'rxjs/operators';
import removeDigits from '../helpers/removeDigits';
import { of } from 'rxjs/observable/of';

export default function ajaxifyCart() {

    Shopify.AjaxifyCart = (function($) {
  
        // Some configuration options.
        var _config = {           
            addToCartBtnLabel:             'Add to cart',
            addedToCartBtnLabel:           'Added',
            addingToCartBtnLabel:          'Adding...',
            soldOutBtnLabel:               'Sold Out',
            howLongTillBtnReturnsToNormal: 1800, // in milliseconds.
            cartCountSelector:             '#CartCount',
            cartTotalSelector:             '.js-cart-total',
            cartDrawerSelector:            '.js-popcart',
            overlaySelector:               '.js-popcart-overlay',
            cartOpenSelector:              '.js-popcart-open',
            cartExitSelector:              '.js-popcart-close',
            cartItemsSelector:             '.js-cart-items',
            cartItemQuantitySelector:      '.js-cart-item-quantity',
            cartItemRemoveSelector:        '.js-cart-item-remove',
            // 'aboveForm' for top of add to cart form, 
            // 'belowForm' for below the add to cart form, and 
            // 'nextButton' for next to add to cart button.
            feedbackPosition:              'nextButton',
            
            // Never need to change these
            addToCartBtnSelector:          '[type="submit"]',
            addToCartFormSelector:         'form[action="/cart/add"]',
            shopifyAjaxAddURL:             '/cart/add.js',
            shopifyAjaxUpdateURL:          '/cart/update.js',
            shopifyAjaxCartURL:            '/cart.js'
        };
        
        function openCart(){
            // Slide draw in from right
            $(_config.cartDrawerSelector).animate({
                "right": "0"
            }, 300);

            $(_config.cartDrawerSelector).addClass('active');

            // Fade in the overlay
            $(_config.overlaySelector).fadeIn('fast'); 

            $('html').addClass('popcart-active');
            $('body').addClass('popcart-active');
        }

        function closeCart(){
            // Slide draw in from right
            $(_config.cartDrawerSelector).animate({
                "right": "-100%"
            }, 300);

            // Fade in the overlay
            $(_config.overlaySelector).fadeOut('fast');

            $(_config.cartDrawerSelector).removeClass('active');
            
            $('html').removeClass('popcart-active');
            $('body').removeClass('popcart-active');
        }
        
        // We need some feedback when adding an item to the cart.
        // Here it is.  
        var _showFeedback = function(success, html, $addToCartForm) {
            $('.ajaxified-cart-feedback').remove();
            var feedback = '<p class="ajaxified-cart-feedback ' + success + '">' + html + '</p>';
            switch (_config.feedbackPosition) {
            case 'aboveForm':
                $addToCartForm.before(feedback);
                break;
            case 'belowForm':
                $addToCartForm.after(feedback);
                break;
            case 'nextButton':
            default:
                $addToCartForm.find(_config.addToCartBtnSelector).after(feedback);
                break;   
            }
            $('.ajaxified-cart-feedback').slideDown();
        };

        var _setText = function($button, label) {
            if ($button.children().length) {
                $button.children().each(function() {
                    if ($.trim($(this).text()) !== '') {
                        $(this).text(label);
                    }
                });
            }
            else {
                $button.val(label).text(label);
            }
        };

        var _init = function() {   
            $(document).ready(function() { 
            
                $(_config.addToCartFormSelector).submit(function(e) {
                    e.preventDefault();
                    var $addToCartForm = $(this);
                    var $addToCartBtn = $addToCartForm.find(_config.addToCartBtnSelector);

                    _setText($addToCartBtn, _config.addingToCartBtnLabel);
                    $addToCartBtn.addClass('disabled').prop('disabled', true);
    
                    // Add to cart.
                    $.ajax({
                        url: _config.shopifyAjaxAddURL,
                        dataType: 'json',
                        type: 'post',
                        data: $addToCartForm.serialize(),
                        success: function(itemData) {    
                            // Re-enable add to cart button.
                            $addToCartBtn.addClass('inverted');
                            _setText($addToCartBtn, _config.addedToCartBtnLabel);
            
                            window.setTimeout(function(){
                                $addToCartBtn.prop('disabled', false).removeClass('disabled').removeClass('inverted');
                                _setText($addToCartBtn,_config.addToCartBtnLabel);
                            }, _config.howLongTillBtnReturnsToNormal);

                            // Update the single item count in the cart
                            $(_config.cartItemQuantitySelector).html(itemData.quantity);     

                            if(itemData.quantity == 1) {
                                $('.js-cart-empty').hide();
                                var price = itemData.price;
                                price = removeDigits(price,2);

                                var cartItemMarkup = `<div class="cart-item">
                                    <div class="cart-item__image">
                                        <a href="">
                                            <img src="${itemData.image}" alt="${itemData.handle}" />
                                        </a>
                                    </div>
                                    <div class="cart-item__details">
                                        <h4 class="cart-item__title">
                                            <a href="${itemData.url}">${itemData.product_title}</a>
                                        </h4>
                                        <p class="cart-item__variant">${itemData.variant_title}</p>
                                        <p class="cart-item__price">&pound;${price} x <span class="js-cart-item-quantity">${itemData.quantity}</span></p>
                                        <div class="js-cart-item-remove cart-item__close" data-id="${itemData.id}">
                                            <svg width="14px" height="14px" viewBox="0 0 14 14" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="square">
                                                    <g transform="translate(2.000000, 2.000000)" fill-rule="nonzero" stroke="#9B9B9B" stroke-width="2">
                                                        <path d="M0,0 L10,10" id="Line-2"></path>
                                                        <path d="M0,0 L10,10" id="Line-2-Copy" transform="translate(5.000000, 5.000000) scale(-1, 1) translate(-5.000000, -5.000000) "></path>
                                                    </g>
                                                </g>
                                            </svg>
                                        </div>
                                    </div>
                                </div>`;

                                $(_config.cartItemsSelector).append(cartItemMarkup);
                            }

                            $.getJSON(_config.shopifyAjaxCartURL, function(cart) {
                                // Update total in cart
                                if ($(_config.cartTotalSelector).length) {
                                    if (typeof Currency !== 'undefined' && typeof Currency.moneyFormats !== 'undefined') {
                                        var newCurrency = '';
                                    
                                        if ($('[name="currencies"]').length) { newCurrency = $('[name="currencies"]').val(); }
                                        else if ($('#currencies span.selected').size()) { newCurrency = $('#currencies span.selected').attr('data-currency'); }
                                        
                                        if (newCurrency) { $(_config.cartTotalSelector).html('<span class=money>' + Currency.convert(cart.total_price, "{{ shop.currency }}", newCurrency) + '</span>'); } 
                                        else { $(_config.cartTotalSelector).html(cart.total_price, "{{ shop.money_format | money_without_trailing_zeros  }}"); }
                                    
                                    } else {
            
                                        $(_config.cartTotalSelector).html('&pound;'+removeDigits(cart.total_price,2));
            
                                    }
                                };
                            });                        
                            openCart();
                        }, 
                        error: function(XMLHttpRequest) {
                                var response = eval('(' + XMLHttpRequest.responseText + ')');
                                response = response.description;
                
                                if (response.slice(0,4) === 'All ') {
                                    _showFeedback('error', response.replace('All 1 ', 'All '), $addToCartForm);
                                    $addToCartBtn.prop('disabled', false);
                                    _setText($addToCartBtn, _config.soldOutBtnLabel);
                                    $addToCartBtn.prop('disabled',true);
                                } else {
                                    _showFeedback('error', response, $addToCartForm);
                                    $addToCartBtn.prop('disabled', false).removeClass('disabled');
                                    _setText($addToCartBtn, _config.addToCartBtnLabel);
                                }
                            }
                        });   
                    return false;    
                });

                // Remove items from cart
                $(_config.cartItemRemoveSelector).each(function(){
                    var $removeBtn = $(this);

                    $removeBtn.on("click touch", function(e) {
                        e.preventDefault();
                        var $removeBtn = $(this);
                        var $itemId = $removeBtn.data('id');
    
                        // Remove from cart via ajax and update totals
                        $.ajax({
                            url: _config.shopifyAjaxUpdateURL,
                            dataType: 'json',
                            type: 'post',
                            data: 'updates['+$itemId+']=0', // set item quantity to equal 0
                            success: function(data) {                         
                                var $cartItem = $removeBtn.closest('.cart-item');
    
                                $.getJSON(_config.shopifyAjaxCartURL, function(cart) {
                                    // show empty cart div if 0 total items in cart
                                    if (cart.item_count == 0) { $('.js-cart-empty').show(); }
                                    

                                    // remove cart item entirely from DOM
                                    $cartItem.remove(); 
    
                                    // Update total in cart
                                    if ($(_config.cartTotalSelector).length) {
                                        if (typeof Currency !== 'undefined' && typeof Currency.moneyFormats !== 'undefined') {
                                            var newCurrency = '';
                                        
                                            if ($('[name="currencies"]').size()) { newCurrency = $('[name="currencies"]').val(); }
                                            else if ($('#currencies span.selected').size()) { newCurrency = $('#currencies span.selected').attr('data-currency'); }
                                            
                                            if (newCurrency) { $(_config.cartTotalSelector).html('<span class=money>' + Currency.convert(cart.total_price, "{{ shop.currency }}", newCurrency) + '</span>'); } 
                                            else { $(_config.cartTotalSelector).html(cart.total_price, "{{ shop.money_format | money_without_trailing_zeros  }}"); }
                                        
                                        } else {
                                            $(_config.cartTotalSelector).html('&pound;'+removeDigits(cart.total_price,2));                
                                        }
                                    };
                                });        
                            }, 
                            error: function(XMLHttpRequest) {
                                    var response = eval('(' + XMLHttpRequest.responseText + ')');
                                    response = response.description;
                                    console.log('AJAX Error: '.response);
                                }
                            });   
                        return false;    
                    });
                    
                });

                // Open the cart drawer on cart button trigger
                $(_config.cartOpenSelector).on("click touch", function(e){
                    e.preventDefault;
                    openCart();
                    return false;
                });     

                // Close the cart drawer on 'close' trigger
                $(_config.cartExitSelector).on("click touch", function(e){
                    e.preventDefault;
                    closeCart();                  
                });    

                // Close cart drawer on overlay click
                $(_config.overlaySelector).on("click touch", function(e){
                    e.preventDefault;
                    closeCart();
                    return false;
                });

                // Close cart drawer on ESC key
                $(document).on('keyup',function(evt) {
                    if($(_config.cartDrawerSelector).hasClass('active')){
                        if (evt.keyCode == 27) {
                            closeCart();
                        }
                    }
                });                        

            });
        };
        return {
            init: function(params) {
                // Configuration
                params = params || {};
    
                // Merging with defaults.
                $.extend(_config, params);
    
                // Action
                $(function() {
                    _init();
                });
            },    
            getConfig: function() {
                return _config;
            }
        }  
    })(jQuery);

    Shopify.AjaxifyCart.init();
}