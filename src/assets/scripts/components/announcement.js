export default function announcment(){
    const el = document.querySelector('.js-announcement');
    const header = document.querySelector('.js-site-header');
    
    setTimeout(function(){
        el.classList.add('announcement-reveal');
        header.classList.add('announcement-reveal');   
    }, 2000);

    el.addEventListener("click", hideAnnouncement);
    el.addEventListener("touch", hideAnnouncement);

    function hideAnnouncement(){
        el.classList.remove('announcement-reveal');
        header.classList.remove('announcement-reveal');
    }
}